package module;


public class Kotik {

    private static int satiety = 10;
    private int prettiness;
    private String name;
    private int weight;
    private String voice;


    private static int count = 0; // переменная экземпляра для счёта кол-ва объектов данного класса

    public Kotik() {
        count++;
    }

    public Kotik(int prettiness, String name, int weight, String voice) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.voice = voice;
        count++;
    }


    public boolean doSomething() {
        if (satiety <= 0) {
            System.out.println("I'm hungry!");
            eat(10);
        } else {
            System.out.println("I don't know what i'm doing, but it's cool!");
            satiety--;
        }
        return true;
    }

    public boolean makeNoise() {
        if (satiety <= 0) {
            System.out.println("I'm hungry!");
            eat(10);
        } else {
            System.out.println("Meow!");
            satiety--;
        }
        return true;
    }

    public boolean sleep() {
        if (satiety <= 0) {
            System.out.println("I'm hungry!");
            eat(10);
        } else {
            System.out.println("I'm sleeping");
            satiety--;
        }
        return true;
    }

    public boolean play() {
        if (satiety <= 0) {
            System.out.println("I'm hungry!");
            eat(10);
        } else {
            System.out.println("Now i'm playing ^.^");
            satiety--;
        }
        return true;
    }

    public boolean chaseMouse() {
        if (satiety <= 0) {
            System.out.println("I'm hungry!");
            eat(10);
        } else {
            System.out.println("I'm going to chase mice");
            satiety--;
        }
        return true;
    }

    public void eat(int x) {
        if (satiety >= 50) { // Здесь мы делаем проверку, чтобы котик не был перекормлен
            System.out.println("It doesn't get into me anymore. Do you want me to burst??!");
        } else {
            satiety += x;
        }
    }


    public void eat(int x, String food) {
        if (satiety >= 50) { // Здесь мы делаем проверку, чтобы котик не был перекормлен
            System.out.println("It doesn't get into me anymore. Do you want me to burst??!");
        } else {
            System.out.println("Wow! It's my favorite food! I really like " + food);
        }
    }

    public void eat() {

        eat(10, "food");
    }


    public void liveAnotherDay() {
        // У каждого метода есть условия, если на момент исполнения  он голоден, то мы кормим его методом eat();
        for (int i = 1; i <= 24; i++) {
            int a = (int) (1 + Math.random() * 5); 
                switch (a) {
                    case 1:
                        sleep();
                        break;
                    case 2:
                        play();
                        break;
                    case 3:
                        chaseMouse();
                        break;
                    case 4:
                        doSomething();
                        break;
                    case 5:
                        makeNoise();
                        break;
                    default:
                        System.out.println("ERROR");
                        break;
                }
            }
        }

    public static int getSatiety() {
        return satiety;
    }

    public static void setSatiety(int satiety) {
        Kotik.satiety = satiety;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public void setPrettiness(int prettiness) {
        this.prettiness = prettiness;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Kotik.count = count;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getVoice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }

    public void setKotik(int prettiness, String name, int weight, String voice) {
        setPrettiness(prettiness);
        setName(name);
        if(weight <= 5) {
            System.out.println("Ты такой худой! Покушай!");
            weight += 10;
            System.out.println(weight);
        }
        setWeight(weight);
        setVoice(voice);
    }
}
