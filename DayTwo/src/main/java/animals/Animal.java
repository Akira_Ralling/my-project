package animals;


import food.Food;

public abstract class Animal {

    private int satiety;
    private int weight;
    private double dangerous;
    private String name;

    public abstract void eat(Food food);

    public Animal(int weight, double dangerous, String name, int satiety) {
        this.weight = weight;
        this.dangerous = dangerous;
        this.name = name;
        this.satiety = satiety;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public double getDangerous() {
        return dangerous;
    }

    public void setDangerous(double dangerous) {
        this.dangerous = dangerous;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSatiety() {
        return satiety;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }
}
