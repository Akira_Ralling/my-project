package animals;

public class Elephant extends Herbivore implements Run {


    public Elephant(int weight, double dangerous, String name, int satiety) {
        super(weight, dangerous, name, satiety);
    }

    @Override
    public void runNow() {

        System.out.println("Elephant is gonna start slowly running");
        setSatiety(getSatiety() - 5);
    }
}
