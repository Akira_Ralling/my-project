import food.Food;
import animals.Animal;
import animals.Voice;

public class Worker {
    public void feed(Animal animal, Food food) {
        animal.eat(food);
    }

    public void getVoice(Voice voice) {
        voice.castVote();
    }
}
